#!/bin/sh
# CONFIG
DIR="ppchecker"
MAIN="AsciiShop"
PAGER="less"

# SYS-VARS
MISMATCHED=""
EXECUTIONERROR="false"
COMPILATIONERROR="false"

if [ "$1" = "" ]; then
    cmd=0
else
    cmd="$1"
fi

# HELP
if [ "$cmd" = "h" -o "$cmd" = "help" ]; then
    echo "This script compiles all *.java-files in the current direcotry into '$DIR'.
Then all input-files (.*i[0-9]*) are compared to the appropriate output-files (.*o[0-9]*).
If there is a mismatch the results get presented using the less-pager.
"
    echo "commands: {h}elp, {d}iff, {c}lear, (g) [int]0,1,2,..,99,.."
    echo "      help: show this message"
    echo "      diff: just show the diff-result, without creating/calculating any new files"
    echo "      clear: remove directory '$DIR' and all containing files"
    echo "      g: skip compiling and run the program right away - can followed by a space and a number (define which test to run"
    echo "      [0-9]*: define which test to run (0 = all)"
    echo ""
    echo "less-usage: use 'man less' to get more info"
    echo "      to go to the next diff-file (if there is another) use ':n'/':p'"
    echo "      to scroll use arrow-keys or j/k ( Ctr-U / Ctr-D )"

# CLEAR
elif  [ "$cmd" = "c" -o "$cmd" = "clear" ]; then
    if [ -d "$DIR" ]; then
        cd "$DIR"
        rm *
        cd ..
        rmdir "$DIR"
        echo "removed $DIR and its content"
    else
        echo "nothing to remove"
    fi

# DIR CREATION
elif [ "$cmd" != "h" -a "$mcd" != "help" -a "$cmd" != "c" -a "$cmd" != "clear" -a "$cmd" != "d" -a "$cmd" != "diff" ]; then #-a $cmd -ge 0 ]; then
    if [ ! -d "$DIR" ]; then
        mkdir $DIR
        echo "created dir: $DIR"
    else
        echo "dir exists: $DIR"
    fi

# COMPILATION
    if [ -d "$DIR" ]; then
        if [ "$cmd" != "g" ]; then
            compilation=$(javac -d $DIR ./*.java 2>&1)
            skip="false"
        else
            if [ "$2" != "" ]; then
                cmd=$2
            else
                cmd=0
            fi
            compilation=true
            skip="true"
        fi
        if $compilation; then
            if [ "$skip" = "false" ]; then
                echo "java-files compiled into $DIR"
            else
                echo "skipped: java-file compilation"
            fi

# EXECUTION w/ i-&o-files
            if [ $cmd -eq 0 ]; then
                iffs=*i[0-9]*
                offs=*o[0-9]*
            else
                iffs=*i*"$cmd"
                offs=*o*"$cmd"
            fi
            for iff in ${iffs}; do 
                for off in ${offs}; do
                    i="${iff##*i}"
                    o="${off##*o}"
                    if [ $i = $o ]; then    # check if there is a o* for the i*
                        if execution=$(java -Duser.language=en -Duser.country=US -cp $DIR $MAIN < $iff > $DIR/o$o 2>&1); then
                            echo "exe  [$MAIN]: $iff"
# DIFF
                            if diff=$(diff -sypbBt $DIR/o$o $off > $DIR/d$o 2>&1); then
                                echo "diff [match]: d$o"
                            else
                                echo "diff [mismatch]: d$o"
                                MISMATCHED="$MISMATCHED d${o}"
                            fi
                        else
                            echo "exe  [error]: $o"
                            echo $(cat "$DIR/o$o")
                            EXECUTIONERROR="true"
                        fi
                    fi
                done
            done
        else
            echo "compilation[error]:"
            echo "$compilation"
            COMPILATION="true"
        fi
    else
        echo "missing dir: $DIR"
    fi
fi

# FETCH OUTPUT
if [ -d "$DIR" ]; then
    cd "$DIR"

    if [ "$cmd" = "d" -o "$cmd" = "diff" ]; then
        dff=*d[0-9]*
    else
        dff="$MISMATCHED"
    fi

    # OUTPUT
    if [ "$cmd" = "d" -o "$cmd" = "diff" -o "$MISMATCHED" != "" ]; then
        dff=$(echo "$dff" | sed -e 's/^[ \t]*//')
        $PAGER $dff
    fi
fi

# SUCCESS
if [ "$cmd" != "c" -a "$cmd" != "clear" -a "$cmd" != "h" -a "$cmd" != "help" -a "$cmd" != "d" -a "$cmd" != "diff" -a "$MISMATCHED" = "" ]; then
    if [ "$EXECUTIONERROR" = "false" -a "$COMPILATIONERROR" = "false" ]; then
        echo ""
        echo "SUCCESS!"
    else
        echo ""
        echo "UNKNWON! - error"
    fi
fi
if [ "$MISMATCHED" != "" ]; then
    c=0
    for m in $MISMATCHED; do
       c=${c+1}
    done
    echo ""
    echo "$c MISMATCHED!"
fi
echo ""

