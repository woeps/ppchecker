# ppChecker 1.0

## About
Well, I got annoyed doing this kind of tests manually, so I wrote this script.

This is the very first version. - Thoughts and comments are highly appreciated.

## Prerequisites
This script was intendet to be used on linux-systems.
It's written in Shellscript and uses the javac, java, diff and sed command.

After the download, you need to set execution-rights for the file: `chmod +x ppchecker.sh`.
Depending on your system / settings it might be necessary to prefix the script with `./` which will end up like this: `./ppchecker.sh`
To run some tests you need to put the script, your .java-files and the input- & output-files in the same directory.

## Input- / Output-Files
To get started just download the specifications from the pp-website and put the unpacked files into the same directory.
Furthermore you can write your own tests: Just name the them *ixx and *oxx ('*' = doesn't matter which characters [try not to use spaces] / 'xx' = use the same number on both files / 'i' for the inputfile and 'o' for the outpufile

## Getting Started
Executing the script by just typing `ppchecker.sh` will run it in "default mode", which means that all java-files will get compiled into the ppchecker-folder and all input- & output-files will get compared.

Use `ppchecker.sh h` or `ppchecker.sh help` to print a tiny overview of the commands:
    This script compiles all *.java-files in the current direcotry into 'ppchecker'.
    Then all input-files (.*i[0-9]*) are compared to the appropriate output-files (.*o[0-9]*).
    If there is a mismatch the results get presented using the less-pager.

    commands: {h}elp, {d}iff, {c}lear, (g) [int]0,1,2,..,99,..
          help: show this message
          diff: just show the diff-result, without creating/calculating any new files
          clear: remove directory 'ppchecker' and all containing files
          g: skip compiling and run the program right away - can followed by a space and a number (define which test to run
          [0-9]*: define which test to run (0 = all)

    less-usage: use 'man less' to get more info
          to go to the next diff-file (if there is another) use ':n'/':p'
          to scroll use arrow-keys or j/k ( Ctr-U / Ctr-D )
        

*Note, that java gets executed using the us localisation.*

## Java-Implementation
I'm playing around to see if I can implement this script's functionallity into a java-application to make it platform independent (see branch 'java').
This is currently in very early alpha-state (!) and not ready to use! - Still any comment or recommendation is very welcome.
Once again: The java-version is still work in progress and not functional at the moment!
